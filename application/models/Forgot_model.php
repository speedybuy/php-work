<?php
defined('BASEPATH') OR exit('No Direct script access allowed');

Class Forgot_model extends CI_Model{

	public function forgotUser(){
		
		$emailid = (isset($_POST['emailid']) && !empty($_POST['emailid']))?($_POST['emailid']):(null);
		$resultArr = array();
		if(null != $emailid){
		    if(filter_var($emailid, FILTER_VALIDATE_EMAIL) === false){
		        $resultArr = array('status' => '400','message'=>'Email is not a valid email address.','data'=>'emailid');
		    }else{
        		$otp = rand(1000,9999);		
        		$this->load->database();
        		$this->db->set('otp',$otp);
                $result = $this->db->where('email', $emailid);
                $this->db->update('user');
                $afftectedRows = $this->db->affected_rows();
            
            	$query = $this->db->query("SELECT * FROM user where email='$emailid'");
            	$result = $query->num_rows();
            	$this->db->close();
            	
            	if($result > 0)
            	{
                	$this->load->database();
                	$afftectedRows = $this->db->affected_rows();
                	$email=$afftectedRows['email'];
                	$name=$afftectedRows['full_name'];
            		//$pass=base64_decode($afftectedRows['password']);
                    $to=$emailid;
        			// Your subject
        			$subject="Speedybuy - OTP for password change";
        			// From
        			$header="from:Speedybuy<no-reply@speedybuy.com>";
        			// Your message
        			$message="Hello ".$name.", \r\n\n";
        			//$message.="Dear".$name.", \r\n\n";
        			$message.="OTP for password change is:- ".$otp."\r\n\n";
        	        $message.="Use above OTP to change your Speedybuy account password.\r\n\n";
        			$message.="Thanks & Regards\r\n";
        			$message.="SpeedyBuy \r\n";
        			// send email
        			$sentmail = mail($to,$subject,$message,$header);
        
                	$resultArr = array('status' => '200','message'=>'OTP sent successfully');
            	}else{
        		    $resultArr = array('status' => '300','message'=>'This Email or Mobile Number is not registered with us.');
        	    }
		    }
		}else{
		   $resultArr = array('status' => '300','message'=>'Missing Fields emailid.', 'data'=>'emailid'); 
		}     			
		return json_encode($resultArr); 
	}
	
	public function changePasswordByOTP(){
	   
	    $username = (isset($_POST['username']) && !empty($_POST['username']))?($_POST['username']):(null);
	    $otp = (isset($_POST['otp']) && !empty($_POST['otp']))?($_POST['otp']):(null);
	    $newPassword = (isset($_POST['password']) && !empty($_POST['password']))?($_POST['password']):(null);
	    
	    $resultArr = array();
		
		if(null != $username && null != $otp && null != $newPassword){	
    		$this->load->database();
        	$query = $this->db->query("SELECT * FROM user where otp=$otp and (email='$username' or no='$username')");
        	$result = $query->num_rows();
        	$this->db->close();
        	
        	if($result > 0){
        		 //$resultArr = array('status' => '200','message'=>'Login Successfully', 'role'=>$query->result()[0]->role);
        		 $newPassword = base64_encode($newPassword);
        		 $this->load->database();
        		 $this->db->set('otp','0');
        		 $this->db->set('password',$newPassword);
                 $this->db->where("(otp=$otp) AND (email='$username' OR no='$username')");
                 $this->db->update('user');
                 $afftectedRows = $this->db->affected_rows();
                 $this->db->close();
                 if($afftectedRows > 0){
                    //To
                    $to=$afftectedRows['email'];
        			// Your subject
        			$subject="Speedybuy - Password change successfully";
        			// From
        			$header="from:Speedybuy<no-reply@speedybuy.com>";
        			// Your message
        			$message="Hello ".$afftectedRows['full_name'].", \r\n\n";
        			//$message.="Dear".$name.", \r\n\n";
        	        $message.="Your passord for SpeedyBuy account is changed successfully. \r\n\n";
        			$message.="Thanks & Regards\r\n";
        			$message.="Speedy Buy \r\n";
        			// send email
        			$sentmail = mail($to,$subject,$message,$header);
                    
                    $resultArr = array('status' => '200','message'=>'Password change successfully.');
                 }
        	}else{
        		 $resultArr = array('status' => '400','message'=>'Data no available.');
    		}
		}else{
		     $missingFields = array();
		     if(null == $username){
		         array_push($missingFields,'username');
		     }
		     if(null == $newPassword){
		         array_push($missingFields,'password');
		     }
		     if(null == $otp){
		         array_push($missingFields,'otp');
		     }
		    $resultArr = array('status' => '400','message'=>'Missing Fields '.implode(",",$missingFields),'data'=>$missingFields);
		}
		return json_encode($resultArr); 
	}
}
?>