<?php
defined('BASEPATH') OR exit('No Direct script access allowed');

Class FetchPagerImages_Model extends CI_Model{
    
    public function getImages(){
        $count = (isset($_POST['count']) && !empty($_POST['count']))?($_POST['count']):(null);
        $this->load->database();
        $this->db->select('path');
        $this->db->from('pager_image');
        $this->db->order_by('image_number', 'ASC');
        if(null != $count && is_numeric($count) && $count > 0){
            $this->db->limit($count);            
        }
        $result = $this->db->get()->result();
        $this->db->close();
        $result_array = array();
        foreach ($result as $row)
        {
            array_push($result_array,$row->path);
        }
        $result = array('status' => '200', 'message' =>'Success','data'=>$result_array);
        return json_encode($result);
    }
}