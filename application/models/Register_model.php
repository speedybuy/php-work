<?php
date_default_timezone_set("Asia/Kolkata");
defined('BASEPATH') OR exit('No Direct script access allowed');

Class Register_model extends CI_Model{

	public function registerUser(){
		$date=date("Y-m-d h:i:sa");
		
        $name = (isset($_POST['name']) && !empty($_POST['name']))?($_POST['name']):(null);
		$email = (isset($_POST['email']) && !empty($_POST['email']))?($_POST['email']):(null);
		$mobile_no = (isset($_POST['mobile_no']) && !empty($_POST['mobile_no']))?($_POST['mobile_no']):(null);
		$password = (isset($_POST['password']) && !empty($_POST['password']))?($_POST['password']):(null);
		$location = (isset($_POST['location']) && !empty($_POST['location']))?($_POST['location']):(null);

		
		$role = (isset($_POST['role']) && !empty($_POST['role']))?($_POST['role']):(null);
		
		$resultArr = array();
	
		if(!empty($name) && !empty($email) && !empty($mobile_no) && !empty($password) && !empty($location)){
		    if(filter_var($email, FILTER_VALIDATE_EMAIL) === false){
		        $resultArr = array('status' => '400','message'=>'Email is not a valid email address.','data'=>'email');
		    }else if(!preg_match('/\d{10}/',$mobile_no)){
		        $resultArr = array('status' => '400','message'=>'Mobile Number is not a valid.','data'=>'mobile_no');
		    }else{
		        $this->load->database();
    		    $this->db->where('email',$email);
    		   $this->db->where('no',$mobile_no);
            	$query = $this->db->get('user');
        	    $result = $query->num_rows();
        	    $this->db->close();
                	
            	if($result > 0){
            		 $resultArr = array('status' => '300','message'=>'Email or Mobile Number already exist','data'=>array('email,mobile_no'));
            	}else{
            	     $password = base64_encode($password);
            	     if(!isset($role)){
    		             $role = 'user';  
    		         }
            		 $data = array('full_name' => $name,'email' => $email,'no' => $mobile_no,'password' => $password,'location' => $location,'role' => $role,'added_date' => $date);
            		 $this->load->database();
            		 $result = $this->db->insert('user',$data);
            		 $this->db->close();
            		 
            		 if(isset($result)){
            		 	$to=$email;
        		        // Your subject
        				$subject="Speedybuy Account Details";
        		
        				// From
        				$header="from:Speedybuy<no-reply@speedybuy.com>";
        		
        				// Your message
        				$message="Hello, \r\n\n";
        				$message.="Greetings from Speedy Buy!!, \r\n\n";
        				$message.="Congratulations! You have successfully verified your account details, \r\n\n";
        				$message.="Thanks for signing up!, \r\n\n";
        				$message.="Your account has been created, \r\n\n";
        				$message.="Username: ". $email ."  OR  ". $mobile_no ." \r\n\n";
        				$message.="If you don't remember password for this account, Please use forgot password link to reset the account password. \r\n\n";
        				$message.="If you didn't request this, please ignore this email. \r\n\n";
        				$message.="Thanks & Regards\r\n";
        				$message.="Speedy Buy \r\n";
        
        				// send email
        				$sentmail = mail($to,$subject,$message,$header);
        				$resultArr = array('status' => '200','message'=>'User Registration Successful');
            		 }else{
            		 	 $resultArr = array('status' => '300','message'=>'User Registration Unsuccessful');
            		 }
            	}
		    }
		    
		}else{
		    $missingFields = array();
		    if(empty($name)){
		        array_push($missingFields,'name');
		    }
		    if(empty($email)){
		        array_push($missingFields,'email');
		    }
		    if(empty($mobile_no)){
		        array_push($missingFields,'mobile_no');
		    }
		    if(empty($password)){
		        array_push($missingFields,'password');
		    }
		    if(empty($location)){
		        array_push($missingFields,'location');
		    }
		    
		    $resultArr = array('status' => '400','message'=>'Missing Fields '.implode(",",$missingFields), 'data'=>$missingFields);
		}
		
		
		return json_encode($resultArr); 
	}

}