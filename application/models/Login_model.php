<?php
defined('BASEPATH') OR exit('No Direct script access allowed');

Class Login_model extends CI_Model{

	public function loginUser(){
		$username = (isset($_POST['username']) && !empty($_POST['username']))?($_POST['username']):(null);
		$password = (isset($_POST['password']) && !empty($_POST['password']))?($_POST['password']):(null);
	
		$resultArr = array();
		
		if(null != $username && null != $password){	
		    $password = base64_encode($password);
    		$this->load->database();
        	$query = $this->db->query("SELECT * FROM user where password='$password' and (email='$username' or no='$username')");
        	$result = $query->num_rows();
        	$this->db->close();
        	
        	if($result > 0){
        		 $resultArr = array('status' => '200','message'=>'Login Successfully', 'role'=>$query->result()[0]->role);
        	}else{
        		 $resultArr = array('status' => '400','message'=>'Login Failed');
    		}
		}else{
		     $missingFields = array();
		     if(null == $username){
		         array_push($missingFields,'username');
		     }
		     if(null == $password){
		         array_push($missingFields,'password');
		     }
		    $resultArr = array('status' => '400','message'=>'Missing Fields '.implode(",",$missingFields),'data'=>$missingFields);
		}
		return json_encode($resultArr); 
	}
}