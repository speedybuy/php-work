<?php
defined('BASEPATH') OR exit('No Direct script access allowed');

Class FetchProductData_Model extends CI_Model{
    
    public function getProducts(){   
        $shop_id = (isset($_POST['shop_id']) && !empty($_POST['shop_id']))?($_POST['shop_id']):(null);
        $product_name = (isset($_POST['product_name']) && !empty($_POST['product_name']))?($_POST['product_name']):(null);
        $user_long = (isset($_POST['longitude']) && !empty($_POST['longitude']))?($_POST['longitude']):(null);
        $user_lati = (isset($_POST['latitude']) && !empty($_POST['latitude']))?($_POST['latitude']):(null);
        $category = (isset($_POST['category']) && !empty($_POST['category']))?($_POST['category']):(null);
        $subcategory = (isset($_POST['subcategory']) && !empty($_POST['subcategory']))?($_POST['subcategory']):(null);
        $count = (isset($_POST['count']) && !empty($_POST['count']))?($_POST['count']):(20);
               
        $product_data = array();
        if(null != $shop_id){
            $product_data = $this->getProductDataFromShopId($shop_id,$count);
        }elseif (null != $product_name && null != $user_long && null != $user_lati){
            $product_data = $this->getProductByNameAndLocation($product_name, $user_long, $user_lati,$count);
        }elseif (null != $user_long && null != $user_lati){
            $product_data = $this->getProductByLocation($user_long, $user_lati,$count);
        }elseif(null != $category){
            $product_data = $this->getProductByCategory($category,$count);
        }elseif (null != $subcategory){
            $product_data = $this->getProductBySubCategory($subcategory,$count);
        }else {
            $missing_data = array();
            if(null == $shop_id){
                array_push($missing_data,"shop_id");
            }
            if (null == $user_long){
                array_push($missing_data,"longitude");
            }
            if (null == $user_lati){
                array_push($missing_data,"latitude");
            }
            if (null == $product_name){
                array_push($missing_data,"product_name");
            }
            if (null == $category){
                array_push($missing_data,"category");
            }
            $product_data = array('status' => '400','message'=>'Missing Fields '.implode(",",$missing_data), 'data'=>$missing_data);
        }
        return json_encode($product_data);
    }
	   
    public function getProductDataFromShopId($shop_id,$count){
        $product_data = array();
        if(null != $shop_id){
    	    $this->load->database();
    	    $query=$this->db->query("SELECT * FROM product where shop_id='$shop_id' AND quantity > '0' order by offer_percent DESC Limit $count");
    	    $result=$query->num_rows();
    	    $this->db->close();   
    	    $product_data = array();
    	    if($result){
    	         foreach ($query->result() as $row){
    	                array_push($product_data,$row);
                 }
                $product_data = array('status' => '200','message'=>'Success','data'=>$product_data); 
    	    }else{
                  $product_data = array('status' => '400','message'=>'Data not available.');
            }
        }else{
            $product_data = array('status' => '400','message'=>'Missing Fields shop_id.', 'data'=>array('shop_id'));
        }
        return ($product_data);
            
	}
	
	public function getProductsInStock(){
	    $this->load->database();
	    $query=$this->db->query("SELECT * FROM product where quantity!='0' order by offer_percent DESC");
	    $result=$query->num_rows();
	    $this->db->close();   
	    $product_data = array();
	    if($result){
	         foreach ($query->result() as $row){
	                array_push($product_data,$row);
             }
                $product_data = array('status' => '200','message'=>'Success','data'=>$product_data);    
	    }else{
               $product_data = array('status' => '400','message'=>'Data not available.');
        }
        
        return ($product_data); 
	}
	
	public function getProductByNameAndLocation($product_name, $user_long, $user_lati,$count)
	{
	 $this->load->database();
	 $query=$this->db->query("Select * from (SELECT * FROM(
    SELECT shop_id,(((acos(sin(($user_lati*3.142857/180)) * sin((latitude*3.142857/180))+cos(($user_lati*3.142857/180)) * cos((latitude*3.142857/180)) * cos((($user_long - longitude)*3.142857/180))))*180/3.142857)*60*1.1515*1.609344) as distance FROM shop) shop_distance
WHERE distance <= 2) shop_distance_table INNER JOIN product ON shop_distance_table.shop_id = product.shop_id AND (product_name LIKE '%$product_name%' or description LIKE '%$product_name%') order by offer_percent DESC Limit $count");
	 $result=$query->num_rows();
	 $this->db->close();
	 $product_data = array();
	 if($result) {
	    foreach($query->result() as $row){
	        array_push($product_data,$row);
	        
	    } 
	    $product_data = array('status' => '200', 'message' =>'Success','data'=>$product_data);
	 }else{
	     $product_data = array('status' => '400', 'message' => 'Data not available.');
	 }
	    return ($product_data);
	}
	
	public function getProductByLocation($user_long, $user_lati,$count)
	{
	    $this->load->database();
	    $query=$this->db->query("Select * from (SELECT * FROM(
    SELECT shop_id,(((acos(sin(($user_lati*3.142857/180)) * sin((latitude*3.142857/180))+cos(($user_lati*3.142857/180)) * cos((latitude*3.142857/180)) * cos((($user_long - longitude)*3.142857/180))))*180/3.142857)*60*1.1515*1.609344) as distance FROM shop) shop_distance
WHERE distance <= 2) shop_distance_table INNER JOIN product ON shop_distance_table.shop_id = product.shop_id order by offer_percent DESC Limit $count");
	    $result=$query->num_rows();
	    $this->db->close();
	    $product_data = array();
	    if($result) {
	        foreach($query->result() as $row){
	            array_push($product_data,$row);	            
	        }
	        $product_data = array('status' => '200', 'message' =>'Success','data'=>$product_data);
	    }else{
	        $product_data = array('status' => '400', 'message' => 'Data not available.');
	    }
	    return ($product_data);
	}
    
	public function getProductByCategory($category,$count){
	    $this->load->database();
	    $query=$this->db->query("Select * from product where categories LIKE '%$category%' order by offer_percent DESC Limit $count");
	    $result=$query->num_rows();
	    $this->db->close();
	    $product_data = array();
	    if($result) {
	        foreach($query->result() as $row){
	            array_push($product_data,$row);
	        }
	        $product_data = array('status' => '200', 'message' =>'Success','data'=>$product_data);
	    }else{
	        $product_data = array('status' => '400', 'message' => 'Data not available.');
	    }
	    return ($product_data);	    
	}
	
	public function getProductBySubCategory($sub_category,$count){
	    $this->load->database();
	    $query=$this->db->query("Select * from product where subcategories LIKE '%$sub_category%' order by offer_percent DESC Limit $count");
	    $result=$query->num_rows();
	    $this->db->close();
	    $product_data = array();
	    if($result) {
	        foreach($query->result() as $row){
	            array_push($product_data,$row);
	        }
	        $product_data = array('status' => '200', 'message' =>'Success','data'=>$product_data);
	    }else{
	        $product_data = array('status' => '400', 'message' => 'Data not available.');
	    }
	    return ($product_data);
	}
	
	/**
	 * Added this method to get all Subcategories for particular category
	 */
	public function getAllSubcategoriesFromCategory(){
	    $category = (isset($_POST['category']) && !empty($_POST['category']))?($_POST['category']):(null);
	    if(null == $category){
	        return json_encode(array('status' => '400','message'=>'Missing Field category', 'data'=>array("category")));
	    }
	    $this->load->database();
	    $this->db->distinct();
	    $this->db->select('subcategories');
	    $this->db->where("categories", $category);
	    $query = $this->db->get("product");
	    $this->db->close();
	    $result = array();
	    if($query->num_rows() > 0){
	        foreach ($query->result() as $row){
	            if(!empty($row->subcategories)){
	                array_push($result,$row->subcategories);
	            }
	        }
	    }
	    return json_encode(array('status' => '200', 'message' =>'Success','data'=>$result));
	}
}