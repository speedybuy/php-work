<?php
defined('BASEPATH') OR exit('No Direct script access allowed');

Class Cart_Model extends CI_Model{
    
    //Function to get cart details for particular user
    public function getUserCart(){
        $user_name = (isset($_POST['user_name']) && !empty($_POST['user_name']))?($_POST['user_name']):(null);
        $cart_data = array();
        if(null != $user_name){
            $cart_product_id = $this->getUserCartProducts($user_name);
  
            if($cart_product_id != null && count($cart_product_id) > 0){
                $this->load->database();
                $this->db->select("*");
                $this->db->from('product');
                $this->db->or_where_in('product_id', $cart_product_id);
                $cart_data = $this->db->get()->result_array();                
                $cart_data = array('status' => '200','message'=>'Success', 'data'=>$cart_data);
                $this->db->close(); 
            }else{
                $cart_data = array('status' => '400','message'=>'Cart is Empty');
            }   
        }else{
            $cart_data = array('status' => '400','message'=>'Missing Fields UserName', 'data'=>array("user_name"));
        }
        
        return json_encode($cart_data);
    }
    
    //Function to add products in cart for particular user
    public function addProductInUserCart(){
        $user_name = (isset($_POST['user_name']) && !empty($_POST['user_name']))?($_POST['user_name']):(null);
        $products = (isset($_POST['products']) && !empty($_POST['products']))?($_POST['products']):(null);
        
        if($products != null && $user_name != null){
            $products = explode(",",$products);
            $cart_product_id = $this->getUserCartProducts($user_name);
            if(null != $cart_product_id && count($cart_product_id) >0){
                $cart_product_id = array_merge($cart_product_id,$products);
                $this->load->database();
                $this->db->set('products', implode(",",$cart_product_id));
                $this->db->where('no', $user_name);
                $this->db->or_where('email', $user_name);
                $this->db->update('cart');
                $this->db->close();
            }else{
                $this->load->database();
                $this->db->select("no,email");
                $this->db->from("user");
                $this->db->where('no', $user_name);
                $this->db->or_where('email', $user_name);
                $result = $this->db->get()->row();
                if(null != $result){
                    $data = array("no"=>$result->no, "email"=>$result->email, "products"=>implode(",",$products));
                    $this->db->insert('cart', $data);
                }
                $this->db->close();
            }
            $cart_data = array('status' => '200','message'=>'Success');
        }else{
            $missing_data = array();
            if($products == null){
                array_push($missing_data,"products");
            }
            if($user_name == null){
                array_push($missing_data,"user_name");
            }
            
            $cart_data = array('status' => '400','message'=>'Missing Fields '.implode(",",$missing_data), 'data'=>$missing_data);
        }
        return json_encode($cart_data);
    }
    
    public function removeProductFromUserCart(){
        $user_name = (isset($_POST['user_name']) && !empty($_POST['user_name']))?($_POST['user_name']):(null);
        $products = (isset($_POST['products']) && !empty($_POST['products']))?($_POST['products']):(null);
        
        if($products != null && $user_name != null){
            $products = explode(",",$products);
            $cart_product_id = $this->getUserCartProducts($user_name);
            if(null != $cart_product_id && count($cart_product_id) >0){
                $updated_products_id = array();
                foreach ($cart_product_id as $product_id){
                    if(!in_array($product_id,$products)){
                        array_push($updated_products_id,$product_id);
                    }
                }
                
                if(count($updated_products_id) > 0){
                    $this->load->database();
                    $this->db->set('products', implode(",",$updated_products_id));
                    $this->db->where('no', $user_name);
                    $this->db->or_where('email', $user_name);
                    $this->db->update('cart');
                    $this->db->close();
                }else{
                    $this->load->database();
                    $this->db->where('no', $user_name);
                    $this->db->or_where('email', $user_name);
                    $this->db->delete('cart');
                    $this->db->close();
                }
                
                $cart_data = array('status' => '200','message'=>'Product deleted Successfully');
            }else{
                $cart_data = array('status' => '400','message'=>'Cart is Empty.');
            }
        }else{
            $missing_data = array();
            if($products == null){
                array_push($missing_data,"products");
            }
            if($user_name == null){
                array_push($missing_data,"user_name");
            }
            $cart_data = array('status' => '400','message'=>'Missing Fields '.implode(",",$missing_data), 'data'=>$missing_data);
        }
        
        return json_encode($cart_data);
    }
    
    public function clearUserCart(){
        $user_name = (isset($_POST['user_name']) && !empty($_POST['user_name']))?($_POST['user_name']):(null);
        if($user_name != null){
            $cart_product_id = $this->getUserCartProducts($user_name);
            if(null != $cart_product_id){
                $this->load->database();
                $this->db->where('no', $user_name);
                $this->db->or_where('email', $user_name);
                $this->db->delete('cart');
                $this->db->close();
                $cart_data = array('status' => '200','message'=>'Cart cleared Successfully');
            }else {
                $cart_data = array('status' => '200','message'=>'Cart already cleared');
            }
            
        }else{
            $cart_data = array('status' => '400','message'=>'Missing Fields user_name', 'data'=>array("user_name"));
        }
        
        return json_encode($cart_data);
    }
    
    private function getUserCartProducts($user_name){
        $this->load->database();
        $this->db->select('products');
        $this->db->from('cart');
        $this->db->where('email', $user_name);
        $this->db->or_where('no', $user_name);
        $this->db->limit(1);
        $result = $this->db->get()->row();
        $this->db->close();
        if($result != null){
            $result = explode(",",$result->products);
        }
        
        return $result;
    }
}   