<?php
defined('BASEPATH') OR exit('No Direct script access allowed');

Class FetchShopData_Model extends CI_Model{

	public function searchShop(){
	    
	    $user_long = (isset($_POST['longitude']) && !empty($_POST['longitude']))?($_POST['longitude']):(null);
        $user_lati = (isset($_POST['latitude']) && !empty($_POST['latitude']))?($_POST['latitude']):(null);
        $shop_data = array();
         
        if(null != $user_long && null != $user_lati){
    		$this->load->database();
    		$query = $this->db->query("SELECT * FROM shop");
            $result = $query->num_rows();
            $this->db->close();
            
            if(is_numeric($user_long) && is_numeric($user_lati)){
                foreach ($query->result() as $row){
                     $distance = $this->getDistance($user_long, $user_lati, $row->longitude, $row->latitude);
                     if($distance<=2){
                         array_push($shop_data,$row);
                     }
                }
                $shop_data = array('status'=>'200', 'data'=>$shop_data);
            }else{
                if(!is_numeric($user_long)){
                     $shop_data = array('status'=>'400','message' => 'Wrong longitude', 'data'=>'longitude');
                }
                if(!is_numeric($user_lati)){
                     $shop_data = array('status'=>'400','message' => 'Wrong latitude', 'data'=>'latitude');
                }
            }
        }else{
            $missingFields = array();
            if(null == $user_long){
                array_push($missingFields,'longitude');
            }
            if(null == $user_lati){
                array_push($missingFields,'latitude');
            }
            $shop_data = array('status'=>'400', 'message' => 'Missing Fields '.implode(",",$missingFields), 'data'=>$missingFields);
        }
        return json_encode($shop_data);
    }
    
    public function getDistance($user_long, $user_lati, $shop_long, $shop_lati){
        $theta = $user_long - $shop_long;
        $dist = sin(deg2rad($user_lati)) * sin(deg2rad($shop_lati)) +  cos(deg2rad($user_lati)) * cos(deg2rad($shop_lati)) * cos(deg2rad($theta));
        $dist = rad2deg(acos($dist));
        $miles = $dist * 60 * 1.1515;
        return ($miles * 1.609344); //return distance in KM
	}
    	
     	
}