<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {

	public function index()
	{	
		$this->load->model('Register_model');
		
		$response = $this->Register_model->registerUser();
		
		echo $response ;
	}

}
