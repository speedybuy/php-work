<?php

defined('BASEPATH') OR exit('No Direct script access allowed');

class Cart extends CI_Controller {
    
    public function index()
    {
       
    }
    
    public function getUserCart()
    {
       $this->load->model('Cart_Model');
       $response = $this->Cart_Model->getUserCart();
       echo $response ;
    }
    
    public function addProducts()
    {
        $this->load->model('Cart_Model');
        $response = $this->Cart_Model->addProductInUserCart();
        echo $response ;
    }
    
    public function removeProducts()
    {
        $this->load->model('Cart_Model');
        $response = $this->Cart_Model->removeProductFromUserCart();
        echo $response ;
    }
    
    public function clearCart()
    {
        $this->load->model('Cart_Model');
        $response = $this->Cart_Model->clearUserCart();
        echo $response ;
    }
}