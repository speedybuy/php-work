<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ChangePasswordByOTP extends CI_Controller {

	public function index()
	{	
		$this->load->model('Forgot_model');
		$response = $this->Forgot_model->changePasswordByOTP();
		echo $response ;
	}
}
