<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Forgotpassword extends CI_Controller {

	public function index()
	{	
		$this->load->model('Forgot_model');
		$response = $this->Forgot_model->forgotUser();
		echo $response ;
	}
}
