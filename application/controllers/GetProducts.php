<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class GetProducts extends CI_Controller {
    
    public function index()
    {
        $this->load->model('FetchProductData_Model');
        $response = $this->FetchProductData_Model->getProducts();
        echo $response ;
    }
}
