<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class GetPagerImages extends CI_Controller {
    
    public function index()
    {
        $this->load->model('FetchPagerImages_Model');
        $response = $this->FetchPagerImages_Model->getImages();
        echo $response ;
    }
}