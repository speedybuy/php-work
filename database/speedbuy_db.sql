-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 29, 2019 at 09:06 AM
-- Server version: 5.6.39-cll-lve
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `speedbuy_db`
--
CREATE DATABASE IF NOT EXISTS `speedbuy_db` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `speedbuy_db`;

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `product_id` int(11) NOT NULL,
  `shop_id` int(11) NOT NULL,
  `product_name` varchar(200) NOT NULL,
  `description` varchar(100) NOT NULL,
  `image` varchar(200) NOT NULL,
  `quantity` int(11) NOT NULL,
  `added_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`product_id`, `shop_id`, `product_name`, `description`, `image`, `quantity`, `added_date`) VALUES
(1, 1, 'Mogra Rice', '', '', 0, '2019-01-30'),
(3, 2, 'Basmati Rice', '', '', 0, '2019-01-30'),
(4, 3, 'Moong Dal', '', '', 0, '2019-01-30'),
(5, 4, 'Aashirvaad Atta', '', '/application/images/925104048-3562971-1.png', 15, '2019-02-02');

-- --------------------------------------------------------

--
-- Table structure for table `shop`
--

CREATE TABLE `shop` (
  `shop_id` int(20) NOT NULL,
  `shop_name` varchar(200) NOT NULL,
  `email` varchar(150) NOT NULL,
  `mobile_no` bigint(20) NOT NULL,
  `address` varchar(100) NOT NULL,
  `latitude` double NOT NULL,
  `longitude` double NOT NULL,
  `added_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `shop`
--

INSERT INTO `shop` (`shop_id`, `shop_name`, `email`, `mobile_no`, `address`, `latitude`, `longitude`, `added_date`) VALUES
(1, 'Shawarma King', 'ShawarmaKing@gmail.com', 9037467567, 'Nigdi', 18.6488601, 73.7653306, '2019-01-03'),
(2, 'Dmart', 'dmart@mail.com', 89675674645, 'Bhusawal', 21.0574057, 75.771244, '2019-01-05'),
(3, 'STAR Baazar', 'starbaazar@gmail.com', 9028617145, 'Chinchwad', 18.6474111, 73.7834156, '2019-01-21'),
(4, 'Chamunda Super Shop', 'chamunda@gmail.com', 90438665454, 'bhusawal', 21.0567192, 75.7833723, '2019-04-11');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `full_name` varchar(150) NOT NULL,
  `no` bigint(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `otp` int(11) NOT NULL,
  `location` varchar(100) NOT NULL,
  `role` varchar(50) NOT NULL,
  `added_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `full_name`, `no`, `email`, `password`, `otp`, `location`, `role`, `added_date`) VALUES
(1, 'gunesh', 1234567890, 'gunesh@gmail.com', 'Z3VuZXNo', 0, 'pune', 'vendor', '0000-00-00 00:00:00'),
(12, 'rohit deshmukh', 89843029853, 'deshmukhrohit@gmail.com', 'cm9oaXRAMTEz', 0, 'ravet', 'user', '2019-02-06 11:09:45'),
(14, 'Aa', 123456789, 'aa@gmail.com', 'YWE=', 0, 'Aaaaa', 'user', '2019-03-22 02:14:17'),
(15, 'Aa', 1234567890, 'aaa@gmail.com', 'YWFh', 0, 'Aaaa', 'user', '2019-03-22 02:21:22'),
(16, 'Asd', 1234567809, 'asd@gmail.com', 'YXNk', 0, 'Asdasd', 'user', '2019-03-22 02:28:47'),
(17, 'Ashu', 8446816455, 'ashwinichaudhari58@gmail.com', 'S3Jpc2huYWc=', 0, 'Bhusawal', 'user', '2019-03-22 03:01:32'),
(18, 'Ashu', 8446816455, 'ashwinichaudhari19940704@gmail.com', 'S3Jpc2huYWc=', 0, 'Bhusawal ', 'user', '2019-03-27 09:16:49'),
(20, 'Ashu', 9420114233, 'ashwinichaudhari19940704@gmail.com', 'S3Jpc2huYWc=', 0, 'Bsl', 'user', '2019-03-30 12:09:04'),
(21, 'Sujata', 9049785785, 'sujatasarode05@gmail.com', 'cHJhZGhueWE=', 0, 'Akurdi', 'user', '2019-03-30 10:00:12'),
(22, 'Asd', 1234567890, 'ashwinichaudhari19940704@gmail.com', 'S3Jpc2huYWc=', 0, 'Bsl', 'user', '2019-03-30 10:11:47'),
(23, 'Gokul Bhoi', 19595688289, 'businessprofile9595@gmail.com', 'Z29jb29sZ2pt', 0, 'Bhusawal', 'user', '2019-04-02 04:43:48'),
(24, 'sujata', 9078678678, 'sujatasarode05@gmail.com', 'c3VqYXRhMTIxMg==', 0, 'akurdi', 'user', '2019-04-03 01:11:59'),
(25, 'sujata', 1234567891, 'sweetisarode@gmail.com', 'c3VqYXRhMTIxMg==', 0, 'akurdi', 'user', '2019-04-03 02:28:01'),
(26, 'sujata', 1234567892, 'sweetisarode@gmail.com', 'c3VqYXRhMTIxMg==', 0, 'akurdi', 'user', '2019-04-03 02:28:47'),
(27, 'sujata', 1234567890, 'sweetisarode@gmail.com', 'c3VqYXRhMTIxMg==', 0, 'akurdi', 'user', '2019-04-03 02:29:02'),
(28, 'sujata', 1234567897, 'sweetisarode12@gmail.com', 'c3VqYXRhMTIxMg==', 0, 'akurdi', 'user', '2019-04-03 03:16:29'),
(29, 'sujata', 1234567896, 'sweetisarode12@gmail.com', 'c3VqYXRhMTIxMg==', 0, 'akurdi', 'user', '2019-04-03 03:16:56'),
(30, 'Ashu', 123456789, 'ashwinichaudhari19940704@gmail.com', 'S3Jpc2huYWc=', 0, 'Bhusawal ', 'user', '2019-04-08 09:35:01');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`product_id`),
  ADD KEY `shop_id` (`shop_id`);

--
-- Indexes for table `shop`
--
ALTER TABLE `shop`
  ADD PRIMARY KEY (`shop_id`),
  ADD UNIQUE KEY `shop_id` (`shop_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `shop`
--
ALTER TABLE `shop`
  MODIFY `shop_id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `product_ibfk_1` FOREIGN KEY (`shop_id`) REFERENCES `shop` (`shop_id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
